//
//  Transaction.h
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Transaction : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * TransactionID;
@property (nonatomic, retain) NSNumber * TransactionTypeID;
@property (nonatomic, retain) NSDecimalNumber * Amount;
@property (nonatomic, retain) NSDate * TransactionDate;

@end

//
//  ODataRequest.h
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/19/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BZODataRequestDelegate;
@class BZODataRequest;

typedef void (^dataRequestCompleted_t) (BZODataRequest *request, NSDictionary *response);
typedef void (^dataRequestFailed_t) (BZODataRequest *request, NSError* error);

@interface BZODataRequest : NSOperation 

@property (nonatomic, assign) id<BZODataRequestDelegate> delegate;
@property (nonatomic, assign) BOOL gzipEnabled; //Defaulted to YES


@property (nonatomic, strong) NSMutableDictionary *requestHttpHeadersForGet;
@property (nonatomic, strong) NSMutableDictionary *requestHttpHeadersForPost;

@property (nonatomic, copy) NSString *acceptHeader DEPRECATED_ATTRIBUTE; //Use httpHeaders
@property (nonatomic, copy) NSString *authUser;
@property (nonatomic, copy) NSString *authPass;
@property (nonatomic, copy) NSObject *context;
@property (nonatomic, copy) NSString *contextName;
@property (nonatomic, readonly) NSString *servicePath;
@property (nonatomic, assign) BOOL servicePathAlreadyEscaped;
@property (nonatomic, copy) NSNumber *retryCount;



- (NSInteger)submitGETCOUNTRequest:(NSString*)servicePath;

@property (nonatomic, assign) float timeout;


- (void) submitGETRequest:(NSString*)servicePath;
- (void) submitPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems;
- (void) submitPOSTRequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo;
- (void) submitPOSTRequest:(NSString *)servicePath httpBody:(NSData *)httpBody;
- (void) submitMERGERequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems;
- (void) submitMERGERequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo;
- (void) submitDELETERequest:(NSString*)servicePath;

- (void) submitGETRequest:(NSString*)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- (void) submitPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- (void) submitPOSTRequest:(NSString*)servicePath httpBodyInfo:(NSDictionary*)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- (void) submitPOSTRequest:(NSString*)servicePath httpBody:(NSData *)httpBody withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- (void) submitMERGERequest:(NSString *)servicePath httpBodyItems:(NSArray *)httpBodyItems withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- (void) submitMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;
- (void) submitDELETERequest:(NSString *)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock;

+ (BZODataRequest*) operationForGETRequest:(NSString*)servicePath;
+ (BZODataRequest*) operationForPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray *)httpBodyItems;
+ (BZODataRequest*) operationForPOSTRequest:(NSString*)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo;
+ (BZODataRequest*) operationForPOSTRequest:(NSString*)servicePath httpBody:(NSData *)httpBody;
+ (BZODataRequest*) operationForMERGERequest:(NSString*)servicePath httpBodyItems:(NSArray *)httpBodyItems;
+ (BZODataRequest*) operationForMERGERequest:(NSString*)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo;
+ (BZODataRequest*) operationForDELETERequest:(NSString*)servicePath;

@end

@protocol BZODataRequestDelegate <NSObject>
@required
- (void) request:(BZODataRequest*)request didReceiveResponse:(NSDictionary*)response;
- (void) request:(BZODataRequest *)request didFailWithError:(NSError*) error;
- (void) request:(BZODataRequest *)request didReceiveStatus:(NSString*)status statusCode:(NSInteger)statusCode statusIsConsideredFailure:(BOOL)isFailure;

@end

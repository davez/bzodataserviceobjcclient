//
//  BZODataObjectMapper.h
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/20/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BZODataObjectMapperDelegate;

@interface BZODataObjectMapper : NSObject

@property (nonatomic, assign) id<BZODataObjectMapperDelegate> delegate;
@property (nonatomic, strong) NSObject *context;
@property (nonatomic, strong) NSString *contextName;
@property (nonatomic, strong) NSDictionary *fieldMapping;

- (void) populateObject:(NSObject*)object withJSONDictionary:(NSDictionary*)jsonDictionary;
- (void) populateObject:(NSObject *)object withJSONDictionary:(NSDictionary *)jsonDictionary withFieldMapping:(NSDictionary*)mapping;
- (void) populateObject:(NSObject *)object withJSONDictionary:(NSDictionary *)jsonDictionary withFieldMapping:(NSDictionary*)mapping withDateFormat:(NSString *)dateFormat;

- (NSString*) extractNextTokenFromJSONDictionary:(NSDictionary*)jsonDictionary;
- (NSArray*) extractEntitiesFromJSONDictionary:(NSDictionary*)jsonDictionary;
- (NSArray*) extractEntitiesFromJSONDictionary:(NSDictionary*)jsonDictionary withKey:(NSString *)key;

- (NSDictionary*) dictionaryWithObject:(NSObject*)object;
- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields;
- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields mappingFields:(NSDictionary*)mapping;
- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields mappingFields:(NSDictionary*)mapping withDateFormat:(NSString *)dateFormat;

#if DEBUG
@property (nonatomic, assign) BOOL showDebugLogs;
#endif

@end


@protocol BZODataObjectMapperDelegate <NSObject>

- (NSObject *)objectMapper:(BZODataObjectMapper *)mapper objectForType:(NSString *)type withName:(NSString *)relationship fromObject:(NSObject *)object jsonDictionary:(NSDictionary *)jsonDictionary;
- (NSObject *)objectMapper:(BZODataObjectMapper *)mapper objectForCollectionWithName:(NSString *)collectionName fromObject:(NSObject *)object jsonDictionary:(NSDictionary *)jsonDictionary;

@end
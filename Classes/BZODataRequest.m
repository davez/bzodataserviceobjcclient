//
//  ODataRequest.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/19/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import "BZODataRequest.h"
#import "Constants.h"   //added by Dave 031613
#import "BZODataRequest+Tag.h"


@interface BZODataRequest()

@property (nonatomic, strong) NSURLConnection *urlConnection;
@property (nonatomic, assign) BOOL requestWasCancelled;

@property (nonatomic, strong) NSNumber *httpStatusCode;
@property (nonatomic, strong) NSString *httpStatusMessage;
@property (nonatomic, strong) NSString *internalServicePath;

@property (nonatomic, assign) BOOL requestCompleted;
@property (nonatomic, strong) NSMutableData *receivedData;

@property (nonatomic, copy) dataRequestCompleted_t completeBlock;
@property (nonatomic, copy) dataRequestFailed_t failedBlock;

//Properties when used as NSOperation
@property (nonatomic, strong) NSArray *op_params;
@property (nonatomic, strong) NSString *op_submitMethodSelector;

- (NSString*) formattedStringFromDate:(NSDate*) date;
- (NSDictionary*) dictionaryWithFormattedDatesFromDictionary:(NSDictionary*)dictionary;
- (NSArray*) arrayWithFormattedDatesFromArray:(NSArray*)array;
- (BOOL) failureStatusFromStatusCode:(NSInteger)statusCode;

@end

@implementation BZODataRequest

@synthesize delegate;
@synthesize receivedData;
@synthesize gzipEnabled;
@synthesize acceptHeader;
@synthesize requestCompleted;
@synthesize completeBlock = _completeBlock;
@synthesize failedBlock = _failedBlock;
@synthesize authUser;
@synthesize authPass;
@synthesize context;
@synthesize httpStatusCode;
@synthesize httpStatusMessage;
@synthesize internalServicePath;
@synthesize contextName;
@synthesize urlConnection;
@synthesize requestWasCancelled;
@synthesize servicePathAlreadyEscaped;
@synthesize timeout;
@synthesize retryCount;

//Properties for NSOperation
@synthesize op_params;
@synthesize op_submitMethodSelector;

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Initialization and Deallocation

- (id)init {
    self = [super init];
    if (self) {
        
		self.gzipEnabled = YES;
        self.requestWasCancelled = NO;
        self.servicePathAlreadyEscaped = NO;
        self.timeout = 10.0;

        [self setupDefaultGETRequestHttpHeaders];
        [self setupDefaultPOSTRequestHttpHeaders];
    }
    return self;
}

- (void)dealloc {
    
	self.delegate = nil;
	self.receivedData = nil;
	self.op_params = nil;
	self.op_submitMethodSelector = nil;
    self.completeBlock = nil;
    self.failedBlock = nil;
	self.authUser = nil;
	self.authPass = nil;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = nil;
	self.contextName = nil;
    self.urlConnection = nil;
    self.retryCount = nil;
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Properties

- (NSString *)servicePath {
	
	return self.internalServicePath;
}


//--------------------------------------------------------------------------------------------------------------
#pragma mark - ODataRequest

- (NSInteger)submitGETCOUNTRequest:(NSString*)servicePath {
    
    NSURL *url = nil;
    
    if (self.servicePathAlreadyEscaped) {
        
        url = [NSURL URLWithString:servicePath];
    }
    else {
        
        url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //Setup GET Request Header for plain text response
    [request setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    
    NSURLResponse *response = nil;
    
    NSError *error = nil;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString *dataString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSInteger returnValue = -1;
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    
    NSInteger statusCode = [httpResponse statusCode];
    
    if (![self failureStatusFromStatusCode:statusCode]) {
        
        returnValue = [dataString integerValue];
    }
    
    
    return returnValue;

}

- (void) submitGETRequest:(NSString *)servicePath {
	
	[self submitGETRequest:servicePath withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	[self submitPOSTRequest:servicePath httpBodyItems:httpBodyItems withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitPOSTRequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
	
    [self submitPOSTRequest:servicePath httpBodyInfo:httpBodyInfo withCompletionBlock:nil withFailedBlock:nil];
}

- (void)submitPOSTRequest:(NSString *)servicePath httpBody:(NSData *)httpBody {
 
    [self submitPOSTRequest:servicePath httpBody:httpBody withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitMERGERequest:(NSString *)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	[self submitMERGERequest:servicePath httpBodyItems:httpBodyItems withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
	
    [self submitMERGERequest:servicePath httpBodyInfo:httpBodyInfo withCompletionBlock:nil withFailedBlock:nil];
}

- (void)submitDELETERequest:(NSString *)servicePath {
	
	[self submitDELETERequest:servicePath withCompletionBlock:nil withFailedBlock:nil];
}

- (void) submitGETRequest:(NSString *)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
    
    if (self.isCancelled) {
        return;
    }
    
    self.completeBlock = completionBlock;
    self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = servicePath;
    
    NSURL *url = nil;
    
    if (self.servicePathAlreadyEscaped) {
        
        url = [NSURL URLWithString:servicePath];
    }
    else {
        
        url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }

	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:self.timeout];
	
	[request setHTTPMethod:@"GET"];
    
    [self processHeadersForGETRequest:request];
    
//    if (self.httpHeaders) {
//        
//        NSString *accept = [self.httpHeaders objectForKey:@"Accept"];
//        
//        if (!accept) {
//            
//            accept = @"application/json";
//        }
//        
//        [request setValue:accept forHTTPHeaderField:@"Accept"];
//    }
//    else {
//        
//        if (self.gzipEnabled) {
//            
//            [request addValue:@"gzip" forHTTPHeaderField:@"Accepts-Encoding"];
//        }
//        
//        //Setup GET Request Header
//        
//        if (!self.acceptHeader) {
//            
//            self.acceptHeader = @"application/json";
//        }
//        
//        [request setValue:self.acceptHeader forHTTPHeaderField:@"Accept"];
//	}
    
	self.requestCompleted = NO;
	
    if (self.isCancelled) {
        
        return;
    }
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
    self.urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	self.receivedData = [NSMutableData data];
	
	do {
		
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
    
	self.urlConnection = nil;
}
   
- (void) submitPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
	[self submitPOSTRequest:servicePath httpBodyInfo:(NSDictionary*)httpBodyItems withCompletionBlock:completionBlock withFailedBlock:failedBlock];
}

- (void)submitPOSTRequest:(NSString *)servicePath httpBody:(NSData *)httpBody withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
    
    [self submitPOSTRequest:servicePath httpBodyInfo:(NSDictionary *)httpBody withCompletionBlock:completionBlock withFailedBlock:failedBlock];
}

- (void) submitPOSTRequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
    
    if (self.isCancelled) {
        
        return;
    }
    
    self.completeBlock = completionBlock;
    self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = servicePath;
    
    
    NSURL *url = nil;
    
    if (self.servicePathAlreadyEscaped) {

        url = [NSURL URLWithString:servicePath];
    }
    else {
        
        url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:self.timeout];
	
	NSString *httpBody = nil;
	
	if ([httpBodyInfo isKindOfClass:[NSDictionary class]]) {
		
		NSDictionary *dict = [self dictionaryWithFormattedDatesFromDictionary:httpBodyInfo];
		
        NSError *error = nil;
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        
		httpBody = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        [request setHTTPBody:[httpBody dataUsingEncoding:NSUTF8StringEncoding]];
	}
	else if ([httpBodyInfo isKindOfClass:[NSArray class]]) {
		
		NSArray *arr = [self arrayWithFormattedDatesFromArray:(NSArray*)httpBodyInfo];
        
        NSError *error = nil;
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:&error];
        
		httpBody = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        [request setHTTPBody:[httpBody dataUsingEncoding:NSUTF8StringEncoding]];
	}
    else if ([httpBodyInfo isKindOfClass:[NSData class]]) {
        
        [request setHTTPBody:(NSData *)httpBodyInfo];
    }
	
	[request setHTTPMethod:@"POST"];

    [self processHeadersForPOSTRequest:request];
	
	self.requestCompleted = NO;
	
    if (self.isCancelled) {
        
        return;
    }
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	self.urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	self.receivedData = [NSMutableData data];
	
	do {
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
	
	self.urlConnection = nil;
}


- (void)submitMERGERequest:(NSString *)servicePath httpBodyItems:(NSArray*) httpBodyItems withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
	[self submitMERGERequest:servicePath httpBodyInfo:(NSDictionary*)httpBodyItems withCompletionBlock:completionBlock withFailedBlock:failedBlock];
}

- (void)submitMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
    if (self.isCancelled) {
        
        return;
    }
    
	self.completeBlock = completionBlock;
	self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = nil;
	
	NSURL *url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:self.timeout];
    
    [request setHTTPMethod:@"POST"];
    
    if (self.gzipEnabled) {
        
        [request addValue:@"gzip" forHTTPHeaderField:@"Accepts-Encoding"];
    }
    
    //Setup MERGE Request Header
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"MERGE" forHTTPHeaderField:@"X-HTTP-METHOD"];
    
    self.requestCompleted = NO;
    
    NSDictionary *jsonDictionary = [self dictionaryWithFormattedDatesFromDictionary:httpBodyInfo];
    
    NSError *error = nil;
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *httpBody = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
	
	[request setHTTPBody:[httpBody dataUsingEncoding:NSUTF8StringEncoding]];
    
    if (self.isCancelled) {
        
        return;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    self.receivedData = [NSMutableData data];;
	
	do {
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
	
    self.urlConnection = nil;
}

- (void)submitDELETERequest:(NSString *)servicePath withCompletionBlock:(dataRequestCompleted_t)completionBlock withFailedBlock:(dataRequestFailed_t)failedBlock {
	
    if (self.isCancelled) {
        
        return;
    }
    
	self.completeBlock = completionBlock;
	self.failedBlock = failedBlock;
    self.httpStatusCode = nil;
    self.httpStatusMessage = nil;
	self.internalServicePath = servicePath;
	
	NSURL *url = [NSURL URLWithString:[servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
	[request setTimeoutInterval:self.timeout];
	
	[request setHTTPMethod:@"DELETE"];
	
	if (self.gzipEnabled) {
		
		[request addValue:@"gzip" forHTTPHeaderField:@"Accepts-Encoding"];
	}
	
	//Setup GET Request Header
	[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	self.requestCompleted = NO;
	
    if (self.isCancelled) {
        
        return;
    }
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
    self.urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	self.receivedData = [NSMutableData data];
	
	do {
		
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
		
	} while (!self.requestCompleted);
    
	self.urlConnection = nil;
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Class Methods

+ (BZODataRequest *)operationForGETRequest:(NSString *)servicePath {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObject:servicePath];
	
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitGETRequest:));
	
	return request;
}

+ (BZODataRequest *)operationForPOSTRequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyItems, nil];
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitPOSTRequest:httpBodyItems:));
	
	return  request;
}

+ (BZODataRequest *)operationForPOSTRequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyInfo, nil];
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitPOSTRequest:httpBodyInfo:));
	
	return  request;
}

+ (BZODataRequest *)operationForPOSTRequest:(NSString *)servicePath httpBody:(NSData *)httpBody {
    
    BZODataRequest *request = [BZODataRequest new];
    
    request.op_params = @[servicePath, httpBody];
    request.op_submitMethodSelector = NSStringFromSelector(@selector(submitPOSTRequest:httpBody:));
    
    return request;
}

+ (BZODataRequest *)operationForMERGERequest:(NSString*)servicePath httpBodyItems:(NSArray*)httpBodyItems {
	
	BZODataRequest *request = [BZODataRequest new];
    
    request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyItems, nil];
    request.op_submitMethodSelector = NSStringFromSelector(@selector(submitMERGERequest:httpBodyItems:));
    
    return request;
}

+ (BZODataRequest*) operationForMERGERequest:(NSString *)servicePath httpBodyInfo:(NSDictionary *)httpBodyInfo {
    
    BZODataRequest *request = [BZODataRequest new];
    
    request.op_params = [NSArray arrayWithObjects:servicePath, httpBodyInfo, nil];
    request.op_submitMethodSelector = NSStringFromSelector(@selector(submitMERGERequest:httpBodyInfo:));
    
    return request;
}

+ (BZODataRequest *)operationForDELETERequest:(NSString *)servicePath {
	
	BZODataRequest *request = [BZODataRequest new];
	
	request.op_params = [NSArray arrayWithObject:servicePath];
	request.op_submitMethodSelector = NSStringFromSelector(@selector(submitDELETERequest:));
	
	return request;
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - NSURLConnectionDelegate

- (void) connection:(NSURLConnection*) connection didReceiveResponse:(NSURLResponse *)response {
	
	NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
	
	NSInteger statusCode = [httpResponse statusCode];
    self.httpStatusCode = [NSNumber numberWithInt:statusCode];
	self.httpStatusMessage = [NSHTTPURLResponse localizedStringForStatusCode:statusCode];
    
    BOOL consideredFailure = [self failureStatusFromStatusCode:statusCode];
    
    [self.delegate request:self didReceiveStatus:self.httpStatusMessage statusCode:statusCode statusIsConsideredFailure:consideredFailure];
    
    if (consideredFailure) {
        
        if (self.failedBlock) {
            
            self.failedBlock(self, [NSError errorWithDomain:@"Http" code:statusCode userInfo:nil]);
        }
    }
	
}

- (void)connection:(NSURLConnection *) connection didReceiveData:(NSData *)data {
	
    if (!self.isCancelled) {
        
        [self.receivedData appendData:data];
    }
    else {
        
        self.requestWasCancelled = YES;
        [self.urlConnection cancel];
    }
}

- (NSCachedURLResponse *) connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	
	return nil;
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	self.receivedData = nil;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Connection Failed: Error: %@ %@", [error localizedDescription], [[error userInfo]objectForKey:NSURLErrorFailingURLErrorKey]);
	
    if (self.failedBlock) {
        
        self.failedBlock(self, error);
    }
    
	if ([self.delegate respondsToSelector:@selector(request:didFailWithError:)]) {
		
		[self.delegate request:self didFailWithError:error];
	}
    
    self.requestCompleted = YES;
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    if (self.httpStatusCode && [self failureStatusFromStatusCode:[self.httpStatusCode intValue]]) {
        
        NSLog(@"Not executing didReceiveResponse: because of bad status");
    }
    else if (self.requestWasCancelled) {
        
        NSLog(@"Not executing didReceiveResponse: because request was cancelled");
    }
    else if ([self.httpStatusCode integerValue] != 304){
		
		NSDictionary *json = nil;
        
        
        //temp added by dave 031613 to allow the return of raw data to the delegate---------------------
        if([self.contextName isEqualToString:CONTEXT_FILE]){
            
            
            NSDictionary *dictionary=[NSDictionary dictionaryWithObjectsAndKeys:
                                      self.receivedData, @"data",
                                      self.servicePath, @"servicePath",
                                      self.tag, @"attachment",
                                      nil];
            [self.delegate request:self didReceiveResponse:dictionary];
        }
        else{
            
            if (self.receivedData) {
                
                NSError *error = nil;
                
                json = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingAllowFragments error:&error];
                
                if (error != nil) {
                    
                    NSLog(@"Failed to parse json:%@", error);
                    
                    NSString *receivedDataString = [[NSString alloc]initWithData:self.receivedData encoding:NSUTF8StringEncoding];
                    
                    NSLog(@"Raw received Data:%@", receivedDataString);
                    
                    if ([self.delegate respondsToSelector:@selector(request:didFailWithError:)]) {
                        
                        NSError *newError = [[NSError alloc] initWithDomain:error.domain code:error.code userInfo:@{ @"ReceivedDataString" : [[NSString alloc]initWithData:self.receivedData encoding:NSUTF8StringEncoding] }];
                        
                        [self.delegate request:self didFailWithError:newError];
                    }
                }
                
                if (self.completeBlock) {
                    
                    self.completeBlock(self, json);
                }
                
                if ([self.delegate respondsToSelector:@selector(request:didReceiveResponse:)]) {
                    
                    [self.delegate request:self didReceiveResponse:json];
                }
            }
            
        }
    }
    
    self.requestCompleted = YES;
}

- (void) connection:(NSURLConnection*)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    
    
}

- (void) connection:(NSURLConnection*)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
    // only BASIC HTTP Auth supported/tested (iOS supports more, but until we test...)
	if ([[challenge protectionSpace] authenticationMethod] != NSURLAuthenticationMethodHTTPBasic) {
		return ;
	}
	
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
		if (self.authUser && self.authPass) {
			newCredential = [NSURLCredential credentialWithUser:self.authUser
													   password:self.authPass
													persistence:NSURLCredentialPersistenceNone];
			[[challenge sender] useCredential:newCredential
				   forAuthenticationChallenge:challenge];			
		}
		else {
			[[challenge sender] cancelAuthenticationChallenge:challenge];
		}
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
	
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	
	// only BASIC HTTP Auth supported/tested (iOS supports more, but until we test...)
	if ([[challenge protectionSpace] authenticationMethod] != NSURLAuthenticationMethodHTTPBasic) {
		return ;
	}
	
    if ([challenge previousFailureCount] == 0) {
        NSURLCredential *newCredential;
		if (self.authUser && self.authPass) {
			newCredential = [NSURLCredential credentialWithUser:self.authUser
													   password:self.authPass
													persistence:NSURLCredentialPersistenceNone];
			[[challenge sender] useCredential:newCredential
				   forAuthenticationChallenge:challenge];			
		}
		else {
			[[challenge sender] cancelAuthenticationChallenge:challenge];
		}
		
    } else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
	
	
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - NSOperation

-(void) main {
	
	NSAssert(self.op_submitMethodSelector != nil, @"Must use operation operationForGETRequest or operationForPOSTRequest to create ODataRequest");
	
	SEL selector = NSSelectorFromString(self.op_submitMethodSelector);
	
	int paramterCount = [self.op_params count];
	
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
	if (paramterCount == 1) {
		
		[self performSelector:selector withObject:[self.op_params objectAtIndex:0]];
	}
	else if (paramterCount == 2) {
		
		[self performSelector:selector withObject:[self.op_params objectAtIndex:0] withObject:[self.op_params objectAtIndex:1]];
	}
#pragma clang diagnostic pop
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Utility Methods

- (void) setupDefaultGETRequestHttpHeaders {
    
    self.requestHttpHeadersForGet = [@{@"Accept" : @"application/json", @"Accepts-Encoding" : @"gzip"} mutableCopy];
}

- (void) setupDefaultPOSTRequestHttpHeaders {
    
    self.requestHttpHeadersForPost = [@{@"Accept" : @"application/json", @"Accepts-Encoding" : @"gzip", @"Content-Type" : @"application/json"} mutableCopy];
}

- (void) processHeadersForGETRequest:(NSMutableURLRequest *)request {
    
    for (NSString *key in [self.requestHttpHeadersForGet allKeys]) {
        
        NSString *value = [self.requestHttpHeadersForGet objectForKey:key];
        
        [request setValue:value forHTTPHeaderField:key];
    }
}

- (void) processHeadersForPOSTRequest:(NSMutableURLRequest *)request {
    
    for (NSString *key in [self.requestHttpHeadersForPost allKeys]) {
        
        NSString *value = [self.requestHttpHeadersForPost objectForKey:key];
        
        [request setValue:value forHTTPHeaderField:key];
    }
}

- (NSString *) formattedStringFromDate:(NSDate *)date {
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
	
	[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"];
	
	return [formatter stringFromDate:date];
}

- (NSDictionary*) dictionaryWithFormattedDatesFromDictionary:(NSDictionary *)dictionary {
	
	NSMutableDictionary *returnValue = [NSMutableDictionary dictionaryWithDictionary:dictionary];
	
	for (id key in dictionary) {
		
		id value = [dictionary objectForKey:key];
		
		if ([value isKindOfClass:[NSDate class]]) {
			
			NSDate *date = value;
			
			value = [self formattedStringFromDate:date];
			
			[returnValue setValue:value forKey:key];
		}
		else if ([value isKindOfClass:[NSArray class]]) {
			
			[returnValue setValue:[self arrayWithFormattedDatesFromArray:value] forKey:key];
		}
	}
	
	return returnValue;
}

- (NSArray*) arrayWithFormattedDatesFromArray:(NSArray*)array {
	
	NSMutableArray *returnValue = [NSMutableArray arrayWithArray:array];
	
	for (int i = 0; i < [array count]; ++i) {
		
		id value = [array objectAtIndex:i];
		
		if ([value isKindOfClass:[NSDate class]]) {
			
			NSDate *date = value;
			
			value = [self formattedStringFromDate:date];
		
			[returnValue replaceObjectAtIndex:i withObject:value];
		}
	}
	
	return returnValue;
}
					

- (BOOL)failureStatusFromStatusCode:(NSInteger)statusCode {
    
    BOOL failure = NO;
    
    //4xx and 5xx are considered failures..sometimes 3xx, but we'll consider them successful
    
    NSString *status = [[NSNumber numberWithInt:statusCode]stringValue];
    
    char c = [status characterAtIndex:0];
    
    if (c == '4' || c == '5') {
    
        failure = YES;
    }
    
    return failure;
}

@end

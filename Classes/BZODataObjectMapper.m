//
//  BZODataObjectMapper.m
//  DataServiceClient
//
//  Created by Joseph DeCarlo on 9/20/11.
//  Copyright 2011 Baked Ziti, LLC. All rights reserved.
//

#import "BZODataObjectMapper.h"
#import "NSString+camelCase.h"
#import <objc/runtime.h>


@interface BZODataObjectMapper()

- (NSString *)object:(NSObject *)object propertyType:(NSString *)propertyName;
- (BOOL) object:(NSObject *)object property:(NSString *)propertyName typeofClass:(Class)class;
- (NSDate*) dateFromJSONSerializedDate:(NSString*)jsonDate;
- (NSDecimalNumber*) decimalFromJSONSerializedDecimal:(NSString*)jsonDecimal;

@end

@implementation BZODataObjectMapper

@synthesize delegate = _delegate;
@synthesize context = _context;
@synthesize fieldMapping;

#if DEBUG
@synthesize showDebugLogs;
#endif
//--------------------------------------------------------------------------------------------------------------
#pragma mark - Initialization and Deallocation

- (id)init {
    
    self = [super init];
    if (self) {

#if DEBUG
        self.showDebugLogs = NO;
#endif
    }
    
    return self;
}

- (void)dealloc {

	self.delegate = nil;
	self.context = nil;
	self.fieldMapping = nil;
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - BZODataObjectMapper

- (void) populateObject:(NSObject*)object withJSONDictionary:(NSDictionary*)jsonDictionary {    
    
    [self populateObject:object withJSONDictionary:jsonDictionary withFieldMapping:nil];
}

- (NSString*) extractNextTokenFromJSONDictionary:(NSDictionary*)jsonDictionary {
    
    NSString *returnValue = nil;
    
    NSArray *tempArray = [jsonDictionary objectForKey:@"d"];
	
	if (!tempArray) {
        
		tempArray = (NSArray*)jsonDictionary;
	}
    
    
    if ([tempArray isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *tempDict = (NSDictionary*)tempArray;
        
        returnValue = [tempDict objectForKey:@"__next"];
    }
	
	return returnValue;
}

- (NSArray*) extractEntitiesFromJSONDictionary:(NSDictionary*)jsonDictionary {
	
	return [self extractEntitiesFromJSONDictionary:jsonDictionary withKey:@"d"];
}

- (NSArray*) extractEntitiesFromJSONDictionary:(NSDictionary*)jsonDictionary withKey:(NSString *)key {
	
	NSArray *temp = [jsonDictionary objectForKey:key];
	
	if (!temp) {
        
		temp = (NSArray*)jsonDictionary;
	}
    else if ([temp isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *tempDict = (NSDictionary*)temp;
        
        temp = [tempDict objectForKey:@"results"];
        
        if (!temp) {
            
            temp = @[tempDict];
        }
    }
	
	return temp;
}

- (void) populateObject:(NSObject *)object withJSONDictionary:(NSDictionary *)jsonDictionary withFieldMapping:(NSDictionary *)mapping {

	[self populateObject:object withJSONDictionary:jsonDictionary withFieldMapping:mapping withDateFormat:nil];
}

- (void) populateObject:(NSObject *)object withJSONDictionary:(NSDictionary *)jsonDictionary withFieldMapping:(NSDictionary*)mapping withDateFormat:(NSString *)dateFormat {
    
	
	NSDictionary *tempDictionary = [jsonDictionary objectForKey:@"d"];
	
	if (tempDictionary) {
		
		jsonDictionary = tempDictionary;
	}
    
    for (id __strong key in jsonDictionary) {
        
#if DEBUG
        if (self.showDebugLogs) {

            NSLog(@"Found Key:%@", key);
        }
#endif
		
        id value = [jsonDictionary objectForKey:key];
             
        
        id mappingKey = [mapping objectForKey:key];
        
        NSString *selectorString = nil;
        
        if (mappingKey) {
            
            key = mappingKey;
        }
        
        selectorString = [NSString stringWithFormat:@"set%@:", [key capitalize]];
        
        if (value == [NSNull null]) {
            
            value = nil;
        }
        
        
        
        if ([self object:object property:key typeofClass:[NSDate class]]) {
      
                            
#if DEBUG
            if (self.showDebugLogs) {
                    
                NSLog(@"Found Date");
            }
#endif	
			if (value != [NSNull null]) {
				
				if (dateFormat) {
					
					value = [self dateFromDateFormat:dateFormat jsonDate:value];
				}
				else {
					value = [self dateFromJSONSerializedDate:value];
				}
			}
        }
        else if ([self object:object property:key typeofClass:[NSDecimalNumber class]]) {

#if DEBUG
            if (self.showDebugLogs) {
                
                NSLog(@"Found Decimal");
            }
#endif
			if (value != [NSNull null]) {
				
				value = [self decimalFromJSONSerializedDecimal:value];
			}
        }
        else if ([self object:object property:key typeofClass:[NSNumber class]]) {
            
            if (value != [NSNull null] && [value isKindOfClass:[NSString class]]) {
                
                NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
                [f setNumberStyle:NSNumberFormatterDecimalStyle];
                value = [f numberFromString:value];
            }
        }
		else if ([self object:object property:key typeofClass:[NSArray class]] || [self object:object property:key typeofClass:[NSSet class]] || [self object:object property:key typeofClass:[NSOrderedSet class]]) {

#if DEBUG
            if (self.showDebugLogs) {
                
                NSLog(@"Found array");
            }
#endif
			
			if (value != [NSNull null]) {
				if ([self.delegate respondsToSelector:@selector(objectMapper:objectForCollectionWithName:fromObject:jsonDictionary:)]) {
					value = [self.delegate objectMapper:self objectForCollectionWithName:key fromObject:object jsonDictionary:value];
				}
				else {
					value = nil;
				}
			}
		}
        
        if ([value isKindOfClass:[NSDictionary class]]) {
            if (value != [NSNull null]) {
                if ([self.delegate respondsToSelector:@selector(objectMapper:objectForType:withName:fromObject:jsonDictionary:)]) {
                    value = [self.delegate objectMapper:self objectForType:[self object:object propertyType:key] withName:key fromObject:object jsonDictionary:value];
                }
                else {
                    value = nil;
                }
            }
        }
        
        if ([value isKindOfClass:[NSNumber class]]) {
#if DEBUG
            if (self.showDebugLogs) {
            
				NSLog(@"The value:%@ is an NSNumber", value);
			}
#endif
		}
        
        if ([value isKindOfClass:[NSString class]]) {
            
#if DEBUG
            if (self.showDebugLogs) {
				
				NSLog(@"The value:%@ is an NSString", value);
			}
#endif
        }
        

        SEL selector = NSSelectorFromString(selectorString);
        
        if ([object respondsToSelector:selector]) {
         
#if DEBUG
            if (self.showDebugLogs) {
                
                NSLog(@"Executed [%@ %@%@];",[object class], selectorString, value);
            }
#endif
      			
			if (value == [NSNull null]) {
				value = nil;
			}
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [object performSelector:selector withObject:value];
#pragma clang diagnostic pop
        }
		else {
			
#if DEBUG
            if (self.showDebugLogs) {
                
                NSLog(@"Skipping Key:%@", key);
            }
#endif
	
		}
    }

}

- (NSDictionary*) dictionaryWithObject:(NSObject *)object {
	
	return [self dictionaryWithObject:object ignoreFieldNames:nil];
}

- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray *)ignoreFields {
    
    return [self dictionaryWithObject:object ignoreFieldNames:ignoreFields mappingFields:nil];
}

- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields mappingFields:(NSDictionary*)mapping {

    return [self dictionaryWithObject:object ignoreFieldNames:ignoreFields mappingFields:mapping withDateFormat:nil];
}

- (NSDictionary*) dictionaryWithObject:(NSObject *)object ignoreFieldNames:(NSArray*)ignoreFields mappingFields:(NSDictionary*)mapping withDateFormat:(NSString *)dateFormat {
		
	NSMutableDictionary *props = [NSMutableDictionary dictionary];
	
	unsigned int outCount, i;
	
	objc_property_t *properties = class_copyPropertyList([object class], &outCount);
	
	for (i = 0; i < outCount; i++) {
		
		objc_property_t property = properties[i];
		
		NSString *propertyName = [NSString stringWithUTF8String:property_getName(property)];
		
		if ([ignoreFields containsObject:propertyName]) {
			continue;
		}
		
		id propertyValue = [object valueForKey:(NSString *)propertyName];
		if (propertyValue) {
         
            NSString *mappingName = [mapping objectForKey:propertyName];
            
            if (mappingName) {
                propertyName = mappingName;
            }
            
            //Check to see if it is an NSManagedObject
            if ([object respondsToSelector:@selector(entity)]) {
            
                id objID = object;
                
                NSEntityDescription *description = [objID entity];
                
                NSDictionary *attributes = [description attributesByName];
                
                NSAttributeDescription *attributeDescription = [attributes objectForKey:propertyName];
                
                NSAttributeType attributeType = [attributeDescription attributeType];
                
                if (attributeType == NSBooleanAttributeType) {
                    
                    NSNumber *boolValue = [NSNumber numberWithBool:[propertyValue boolValue]];
                    
                    [props setObject:boolValue forKey:propertyName];
                    
                    continue;
                }
                else if (attributeType == NSFloatAttributeType || attributeType == NSDoubleAttributeType || attributeType == NSDecimalAttributeType) {
                    
                    NSDecimalNumber *decimal = [NSDecimalNumber decimalNumberWithString:[propertyValue stringValue]];
                    
                    [props setObject:decimal forKey:propertyName];
                    
                    continue;
                }
                
            }
            
            if ([propertyValue isKindOfClass:[NSDate class]]) {
                
                if (dateFormat) {
                    
                    propertyValue = [self stringFromDate:propertyValue withDateFormat:dateFormat];
                }
                else {
                
                    propertyValue = [self jsonSerializedDateFromDate:propertyValue];
                }
            }
            
            [props setObject:propertyValue forKey:propertyName];
        }
	}
	
	free(properties);
	
	return props;
}



//--------------------------------------------------------------------------------------------------------------
#pragma mark - Utility Methods

- (NSString *)object:(NSObject *)object propertyType:(NSString *)propertyName {
	
    SEL selector = NSSelectorFromString(propertyName);
	
	if (![object respondsToSelector:selector]) {
		
		propertyName = [propertyName toCamelCase];
		selector = NSSelectorFromString(propertyName);
	}
    
    if ([object respondsToSelector:selector]) {
        
        objc_property_t theProperty = class_getProperty([object class], [propertyName UTF8String]);
        
        if (theProperty) {
            
            const char *propertyAttrs = property_getAttributes(theProperty);
            
            NSString *propertyType = [NSString stringWithCString:propertyAttrs encoding:NSUTF8StringEncoding];
            
            NSArray *propNameComponents = [propertyType componentsSeparatedByString:@"\""];
            
            return [propNameComponents objectAtIndex:1];
        }
    }
    
    return nil;
}

- (BOOL) object:(NSObject *)object property:(NSString *)propertyName typeofClass:(Class)class {
    
    return [[self object:object propertyType:propertyName] isEqualToString:NSStringFromClass(class)];
}

- (NSDate *) dateFromDateFormat:(NSString *)dateFormat jsonDate:(NSString *)jsonDate {
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
	
	[formatter setDateFormat:dateFormat];
	
	return [formatter dateFromString:jsonDate];
}

- (NSDate*) dateFromJSONSerializedDate:(NSString *)jsonDate {
    
    // JSON Serialized Date Format:  /Date(1273884549593)/    
	NSString *ticksString = [jsonDate stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"/Date()"]];
	
    // convert milliseconds to seconds
	NSTimeInterval unixTime = ([ticksString doubleValue] / 1000);
    
	NSDate *returnDate = [NSDate dateWithTimeIntervalSince1970:unixTime];
    
	return returnDate;
}

- (NSDecimalNumber*) decimalFromJSONSerializedDecimal:(NSString *)jsonDecimal {
    
    return [NSDecimalNumber decimalNumberWithString:jsonDecimal];
}

- (NSString *) jsonSerializedDateFromDate:(NSDate *)date {
    
    NSTimeInterval unixTime = [date timeIntervalSince1970];
    
    double ticks = unixTime * 1000;
    
    return [NSString stringWithFormat:@"/Date(%@)", [@(ticks)stringValue]];
}

- (NSString *) stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
	
	[formatter setDateFormat:dateFormat];
	
	return [formatter stringFromDate:date];
}

//--------------------------------------------------------------------------------------------------------------
#pragma mark - Properties

@end
